import socket from 'socket.io';
import ConversationService from '../app/Conversation/Services/ConversationService';

export default function (server) {
    const io = socket(server);

    ConversationService.io = io.of('/messenger').on('connection', (client) => {
        console.log('Client has joined the server on namespace messenger!');
        // Join room
        client.on('joinRoom', (data) => {
            console.log(`Client has joined the room ${data}`);
            client.join(data);
        });
    });
    ConversationService.friendIO = io.of('/dynamic-messenger').on('connection', (client) => {
        console.log('Client has joined the server on namespace dynamic messenger');
        // Join room
        client.on('joinDynamicRoom', (data) => {
            console.log(`Client has joined the dynamic room ${data}`);
            client.join(data);
        });
    });
}
