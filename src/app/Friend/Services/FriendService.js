import friendRepository from '../Repositories/FriendRepository';

import Repository from '../../Auth/Repositories/AuthRepository';

class FriendService {
  static service;

  constructor() {
    this.friendRepository = friendRepository.getfriendRepository();
    this.repository = Repository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  // Send a friend request to other user

  async friendRequestService(friendData, userID) {
    let friendInfo;
    if (friendData.type === 'email') {
      friendInfo = await this.repository.getUserByEmail({ email: friendData.info });
    }
    if (friendData.type === 'phoneNumber') {
      friendInfo = await this.repository.getUserByPhone({ phoneNumber: friendData.info });
    }
    // ! Check if already friend
    const checkAlreadyFriend = await this.friendRepository.checkAlreadyFriend(userID, friendInfo.id);
    if (checkAlreadyFriend.length) {
      return 'already';
    }
    // ! Check if self request
    if (friendInfo.id !== userID) {
      const insertData = [{
        user1_id: userID,
        user2_id: friendInfo.id,
        request_id: userID,
        status: 0,
      }, {
        user1_id: friendInfo.id,
        user2_id: userID,
        request_id: userID,
        status: 0,
      }];
      await this.friendRepository.sendRequest(insertData);
    } else return 'self';
    return 'good';
  }

  // Add friend and unfriend

  async acceptFriendService(userID, friendID) {
    await Promise.all([
      this.friendRepository.acceptRequest(userID, friendID),
      this.friendRepository.acceptRequest(friendID, userID),
    ]);

    // Turn into array to insert into MongoDB
    const ObjectIDs = await this.friendRepository.getObjectID([userID, friendID]);
    const UserObjectIDs = ObjectIDs.map((ObjectID) => ObjectID['_id']);
    // Check if conversation between two people already exists
    await this.friendRepository.addFriendChatMongo(UserObjectIDs);
  }

  async unfriendService(userID, friendID) {
    await Promise.all([
      await this.friendRepository.removeFriend(userID, friendID),
      await this.friendRepository.removeFriend(friendID, userID),
    ]);
  }

  friendService(userID) {
    return this.friendRepository.friendRepository(userID);
  }

  async friendChatServiceMongo(userID) {
    const UserObjectID = await this.friendRepository.getObjectID(userID);
    const filteredChats = await this.friendRepository.friendChatRepositoryMongo(UserObjectID);
    return filteredChats;
  }

  groupChatService(userID) {
    return this.friendRepository.groupChatRepository(userID);
  }
}

export default FriendService;
