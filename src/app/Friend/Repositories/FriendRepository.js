import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

import model from '../../../database/models/index';

import moment from 'moment';
class FriendRepository extends BaseRepository {
  static friendRepository;

  static getfriendRepository() {
    if (!this.friendRepository) {
      this.friendRepository = new this();
    }
    return this.friendRepository;
  }

  getTableName() {
    return 'friendlists';
  }

  // Send a friend request to other user

  checkAlreadyFriend(userID, friendID) {
    return this.listBy({
      user1_id: userID,
      user2_id: friendID,
      status: 1,
    });
  }

  sendRequest(data) {
    return this.create(data);
  }

  // Add friend and unfriend

  acceptRequest(firstUser, secondUser) {
    return this.update({
      user1_id: firstUser,
      user2_id: secondUser,
    }, {
      status: 1,
    });
  }

  getObjectID(userIDs) {
    return model.User.find({ id: { $in: userIDs } }).select('_id');
  }

  addFriendChatMongo(UserObjectIDs) {
    model.Conversation.find({
      userIds: UserObjectIDs,
    }, (err, docs) => {
       if (docs.length === 0) { // If user added this user, it will not create a new conversation
        model.Conversation.create({
          groupName: 'Private Message',
          description: 'Private Message',
          latestMessage: 'You got new friend! Chat something!',
          latestSender: '',
          userIds: UserObjectIDs,
        });
      }
    });
  }

  removeFriend(userID, friendID) {
    return this.delete({
      user1_id: userID,
      user2_id: friendID,
      status: 1,
    });
  }

  friendRepository(userID) {
    return this.listJoinBy({
      user1_id: userID,
    });
  }

  friendChatRepositoryMongo(UserObjectID) {
    return model.Conversation
    .find({ userIds: { $elemMatch: { $eq: UserObjectID[0]._id } } })
    .select('groupName description latestMessage latestSender updatedAt')
    .populate('userIds', 'fullName avatar id')
    .sort([['updatedAt', -1]]);
  }
}

export default FriendRepository;
