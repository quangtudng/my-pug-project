import BaseController from '../../../infrastructure/Controllers/BaseController';

import Service from '../Services/FriendService';

class FriendController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  // Send a friend request to other user

  async friendRequestMethod(req, res) {
    const result = await this.service.friendRequestService(req.body, req.session.user.id);
    res.json(result);
  }

  // Add friend and unfriend

  friendAcceptMethod(req, res) {
    this.service.acceptFriendService(req.session.user.id, req.body.profileID);
    res.json('good');
  }

  unfriendMethod(req, res) {
    this.service.unfriendService(req.session.user.id, req.body.profileID);
    res.json('good');
  }

  // Query current friends

  async getCurrentFriendMethod(req, res) {
    const friends = await this.service.friendService(req.session.user.id);
    return res.json(friends);
  }

  async getCurrentChatMethod(req, res) {
    const chats = await this.service.friendChatServiceMongo(req.session.user.id);
    return res.json({
      chats,
      currentUser: req.session.user.id,
    });
  }

  //

  async getFriendGroupMethod(req, res) {
    const userID = req.session.user.id;
    const groups = await this.service.groupChat(userID);
    return res.json(groups);
  }

  async getCurrentRequestMethod(req, res) {
    const { user } = req.session;
    const requests = await this.service.friendService(req.session.user.id);
    return res.json({
      requests,
      user,
    });
  }

  async showIndex(req, res) {
    const { user } = req.session;
    const chats = await this.service.friendChatServiceMongo(req.session.user.id);
    res.render('app/conversation/index', { user, chats });
  }
}

export default FriendController;
