import express from 'express';
import FriendController from '../Controllers/FriendController';

import { verifyAuthentication } from '../../Auth/Middlewares/AuthMiddleware';


// Declare object
const friendController = new FriendController();

const router = express.Router();

router.post('/add-friend', verifyAuthentication, friendController.callMethod('friendRequestMethod'));

router.post('/accept-friend', verifyAuthentication, friendController.callMethod('friendAcceptMethod'));

router.post('/unfriend', verifyAuthentication, friendController.callMethod('unfriendMethod'));

router.post('/get-current-friend', verifyAuthentication, friendController.callMethod('getCurrentFriendMethod'));

export default router;
