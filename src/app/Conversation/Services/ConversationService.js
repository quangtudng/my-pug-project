import ConversationRepository from '../Repositories/ConversationRepository';

import FriendRepository from '../../Friend/Repositories/FriendRepository';

import AuthRepository from '../../Auth/Repositories/AuthRepository';

class ConversationService {
  static service;

  static io;

  static friendIO;

  constructor() {
    this.repository = ConversationRepository.getRepository();
    this.AuthRepository = AuthRepository.getRepository();
    this.friendRepository = FriendRepository.getfriendRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  searchUserService(data) {
    return this.repository.getUserByFullNameMongo(data);
  }

  async groupAddService(data) {
    const ObjectIDs = await this.friendRepository.getObjectID(data.users);
    const UserObjectIDs = ObjectIDs.map((ObjectID) => ObjectID['_id']);
    await this.repository.groupAdd(data, UserObjectIDs);
  }

  async sendMessageService(data, senderInfo) {
    // Remove security information
    const roomMembers = JSON.parse(data.members);
    const senderInfoFixed = (({ id, fullName }) => ({ id, fullName }))(senderInfo);
    await Promise.all([
      this.repository.sendMessageRepositoryMongo(data, senderInfo),
      this.repository.updateLatestMessageRepositoryMongo(data, senderInfo),
    ]);
    ConversationService.io.to(data.roomID).emit('incoming-message', { message: data.message, senderInfoFixed });
    for (let i = 0; i < roomMembers.length; i += 1) {
      if (roomMembers[i] !== senderInfo.id) {
        ConversationService.friendIO.to(roomMembers[i]).emit('friend-incoming-message', { roomID: data.roomID, message: data.message, sender: senderInfoFixed });
      }
    }
  }

  receiveLatestTimeService(roomID) {
    return this.repository.receiveLatestTimeRepositoryMongo(roomID);
  }

  friendMessageServiceMongo(roomID) {
    return this.repository.friendMessageRepositoryMongo(roomID);
  }
}

export default ConversationService;
