import express from 'express';
import ConversationController from '../Controllers/ConversationController';
import FriendController from '../../Friend/Controllers/FriendController';
import { verifyAuthentication } from '../../Auth/Middlewares/AuthMiddleware';

const router = express.Router();
// Declare Object
const friendController = new FriendController();
const conversationController = new ConversationController();

router.post('/search-user', verifyAuthentication, conversationController.callMethod('searchUserMethod'));

router.post('/group-add', verifyAuthentication, conversationController.callMethod('groupAddMethod'));

router.post('/get-current-chat', verifyAuthentication, friendController.callMethod('getCurrentChatMethod'));

router.post('/get-current-request', verifyAuthentication, friendController.callMethod('getCurrentRequestMethod'));

router.post('/get-friend-group', verifyAuthentication, friendController.callMethod('getFriendGroupMethod'));

router.post('/send-message', verifyAuthentication, conversationController.callMethod('sendMessageMethod'));

router.get('/t/:chat_slug', verifyAuthentication, conversationController.callMethod('showChatIndex'));

export default router;
