import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/ConversationService';
import FriendService from '../../Friend/Services/FriendService';

class ConversationController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
    this.FriendService = FriendService.getService();
    }

  async searchUserMethod(req, res) {
    const searchResults = await this.service.searchUserService(req.body.fullName);
    return res.json({ searchResults, user: req.session.user.id });
  }

  groupAddMethod(req, res) {
    const data = req.body;
    data.users = JSON.parse(data.users);
    data.users.push(req.session.user.id);
    this.service.groupAddService(data);
    return res.json('good');
  }

  async showChatIndex(req, res) {
    const { user } = req.session;
    const roomID = req.params.chat_slug;
    const chats = await this.FriendService.friendChatServiceMongo(req.session.user.id);
    try {
      const messages = await this.service.friendMessageServiceMongo(roomID);
      return res.render('app/conversation/index', { user, chats, allMessages: messages });
    } catch (e) {
      return res.sendStatus(404);
    }
  }

  async sendMessageMethod(req, res) {
    await this.service.sendMessageService(req.body, req.session.user);
    const updatedTime = await this.service.receiveLatestTimeService(req.body.roomID);
    return res.json(updatedTime);
  }
}

export default ConversationController;
