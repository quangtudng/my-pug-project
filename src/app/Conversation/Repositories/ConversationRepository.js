import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

import model from '../../../database/models/index';

class ConversationRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }
    return this.repository;
  }

  getTableName() {
    return 'conversations';
  }

  getUserByFullNameMongo(data) {
    const regex = new RegExp(data, 'i');
    return model.User.find({ fullName: regex });
  }

  groupAdd(data, UserObjectIDs) {
    model.Conversation.create({
      groupName: data.groupName,
      description: data.description,
      userIds: UserObjectIDs,
    });
  }

  updateLatestMessageRepositoryMongo(data, senderInfo) {
    return model.Conversation.updateOne({
      _id: data.roomID,
    }, {
       latestMessage: data.message, latestSender: senderInfo.fullName,
    });
  }

  sendMessageRepositoryMongo(data, senderInfo) {
    return model.Message.create({
      content: data.message,
      conversationID: data.roomID,
      senderID: senderInfo.id,
      senderName: senderInfo.fullName,
    });
  }

  receiveLatestTimeRepositoryMongo(roomID) {
    return model.Conversation.find({ _id: roomID }, 'updatedAt -_id');
  }

  friendMessageRepositoryMongo(roomID) {
    return model.Message.find({
      conversationID: roomID,
    });
  }
}

export default ConversationRepository;
