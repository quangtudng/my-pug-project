import admin from '../../../config/firebase';

export function verifyAuthentication(req, res, next) {
    if (!req.session.user) {
        return res.redirect('/login');
    }
    return next();
}

export function verifyNotAuthentication(req, res, next) {
    if (!req.session.user) {
        return next();
    }
    return res.redirect('/');
}

export function verifyIDToken(req, res, next) {
    admin.auth().verifyIdToken(req.body.idToken)
    .then((decodedToken) => {
        const { uid } = decodedToken;
        return next();
    }).catch((error) => {
        console.log(error);
        
        res.json('badToken');
    });
}
