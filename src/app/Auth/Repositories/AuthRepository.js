import getSlug from 'speakingurl';

import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';

import model from '../../../database/models/index';

class AuthRepository extends BaseRepository {
  static repository;

  static getRepository() {
    if (!this.repository) {
      this.repository = new this();
    }
    return this.repository;
  }

  getTableName() {
    return 'users';
  }

  // Register/Login by phone number

  registerPhoneRepository(data) {
    return this.create({
      fullName: data.fullName,
      user_avatar: 'unknown',
      email: 'unknown',
      phoneNumber: data.phoneNumber,
      password: 'unknown',
      city: 'unknown',
      description: 'unknown',
      slug: 'unknown',
    });
  }

  registerUserMongo(user) {
    model.User.create({
      id: user.id,
      fullName: user.fullName,
      avatar: user.user_avatar,
    });
  }

  getUserByPhoneNumber(data) {
    return this.getBy({
      phoneNumber: data.phoneNumber,
    });
  }

  // Register by Email/Login

  registerEmailRepository(data) {
    return this.create({
      fullName: data.fullName,
      user_avatar: 'unknown',
      email: data.email,
      phoneNumber: 'unknown',
      password: data.password,
      city: 'unknown',
      description: 'unknown',
      slug: getSlug(data.fullName + Date.now()),
    });
  }

  getUserByEmail(data) {
    return this.getBy({
      email: data.email,
    });
  }

  loginEmailRepository(data) {
    return this.getBy({
      email: data.email,
      password: data.password,
    });
  }

  updatePhone(data) {
    return this.update({
      email: data.email,
    }, {
      phoneNumber: data.phoneNumber,
    });
  }

  getUserByPhone(data) {
    return this.getBy({
      phoneNumber: data.phoneNumber,
    });
  }

  getUserByFullName(data) {
    return this.searchLike('fullName', data);
  }
}

export default AuthRepository;
