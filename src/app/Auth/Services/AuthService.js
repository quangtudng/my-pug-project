import admin from '../../../config/firebase';

import Repository from '../Repositories/AuthRepository';

class AuthService {
  static service;

  constructor() {
    this.repository = Repository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  // Register/Login by phone number

  async registerPhoneService(data) {
    await this.repository.registerPhoneRepository(data);
    const newUser = await this.repository.getUserByPhoneNumber(data);
    await this.repository.registerUserMongo(newUser);
  }

  async loginPhoneService(data, req) {
    const user = await this.repository.getUserByPhoneNumber(data);
    if (user) {
      req.session.user = user;
      req.session.save();
    }
  }

  // Register/Login by email

  async registerEmailService(data) {
    await this.repository.registerEmailRepository(data);
    const newUser = await this.repository.getUserByEmail(data);
    await this.repository.registerUserMongo(newUser);
  }

  async loginEmailService(data) {
    return this.repository.loginEmailRepository(data);
  }

  async updatePhoneService(data) {
    // ! Update User phoneNumber on local DB
    await this.repository.updatePhone(data);
    // ! Update User phoneNumber on Firebase
    admin.auth().updateUser(data.uid, {
      phoneNumber: data.phoneNumber,
    });
  }

  async getUserInfoService(data) {
    let result = await this.repository.getUserByEmail(data);
    result = {
      id: result.id,
      email: result.email,
      fullName: result.fullName,
    };
    return result;
  }
}

export default AuthService;
