import express from 'express';
import Controller from '../Controllers/AuthController';
import { verifyAuthentication, verifyNotAuthentication, verifyIDToken } from '../Middlewares/AuthMiddleware';

const router = express.Router();

const controller = new Controller();

router.route('/register-phone')
.all(verifyNotAuthentication)
.get(controller.registerPhoneView)
.post(controller.callMethod('registerPhoneMethod'));

router.route('/login-phone')
.all(verifyNotAuthentication)
.get(controller.loginPhoneView)
.post(verifyIDToken, controller.callMethod('loginPhoneMethod'));

router.route('/register')
  .all(verifyNotAuthentication)
  .get(controller.registerMailView)
  .post(controller.callMethod('registerEmailMethod'));

router.route('/login')
  .all(verifyNotAuthentication)
  .get(controller.loginEmailView)
  .post(verifyIDToken, controller.callMethod('loginEmailMethod'));


router.post('/update-info', verifyAuthentication, controller.callMethod('updatePhoneNumberMethod'));

router.post('/get-user-info', verifyAuthentication, controller.callMethod('getUserInfoMethod'));

router.get('/reset-password', verifyNotAuthentication, controller.resetPassword);

router.get('/logout', verifyAuthentication, controller.logoutMethod);

export default router;
