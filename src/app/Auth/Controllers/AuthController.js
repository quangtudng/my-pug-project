import BaseController from '../../../infrastructure/Controllers/BaseController';
import Service from '../Services/AuthService';

class AuthController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  registerPhoneView(req, res) {
    return res.render('app/auth/register-phone');
  }

  registerPhoneMethod(req, res) {
    this.service.registerPhoneService(req.body);
    return res.json('good');
  }

  loginPhoneView(req, res) {
    return res.render('app/auth/login-phone');
  }

  loginPhoneMethod(req, res) {
    this.service.loginPhoneService(req.body, req);
    return res.json('good');
  }

  registerMailView(req, res) {
    return res.render('app/auth/register-email');
  }

  registerEmailMethod(req, res) {
    this.service.registerEmailService(req.body);
    return res.json('good');
  }

  loginEmailView(req, res) {
    return res.render('app/auth/login-email');
  }

  async loginEmailMethod(req, res) {
    const user = await this.service.loginEmailService(req.body);
    if (!user) {
      return res.json('error');
    }
    while (typeof req.session.user === 'undefined') {
      req.session.user = user;
      req.session.save();
    }
    return res.json('good');
  }

  resetPassword(req, res) {
    return res.render('app/reset-password');
  }

  logoutMethod(req, res) {
    while (typeof req.session.user !== 'undefined') {
      delete req.session.user;
    }
    return res.redirect('/login');
  }

  updatePhoneNumberMethod(req, res) {
    const data = req.body;
    this.service.updatePhoneService(data);
    return res.json('good');
  }

  async getUserInfoMethod(req, res) {
    const data = req.body;
    const result = await this.service.getUserInfoService(data);
    return res.json(result);
  }
}

export default AuthController;
