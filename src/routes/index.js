import express from 'express';
import path from 'path';
import multer from 'multer';
import authRouter from '../app/Auth/Routes/routes';
import conversationRouter from '../app/Conversation/Routes/routes';
import friendRouter from '../app/Friend/Routes/routes';
import FriendController from '../app/Friend/Controllers/FriendController';
import { verifyAuthentication } from '../app/Auth/Middlewares/AuthMiddleware';

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage });

// Declare object
const friendController = new FriendController();

const router = express.Router();
router.use(authRouter);
router.use(conversationRouter);
router.use(friendRouter);
router.get('/', verifyAuthentication, friendController.callMethod('showIndex'));

export default router;
