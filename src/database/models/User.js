import mongoose from 'mongoose';

const schema = new mongoose.Schema({
  id: Number,
  fullName: String,
  avatar: String,
}, {
  timestamps: true,
});
export default mongoose.model('users', schema);
