import mongoose from 'mongoose';

const { Schema } = mongoose;

const schema = new mongoose.Schema({
  content: {
    type: String,
    required: true,
    trim: true,
  },
  conversationID: {
    type: Schema.Types.ObjectId,
    ref: 'Conversation',
  },
  senderID: Number,
  senderName: String,
}, {
  timestamps: true,
});
export default mongoose.model('messages', schema);
