import mongoose, { Schema } from 'mongoose';

const schema = new mongoose.Schema({
  groupName: String,
  description: String,
  latestMessage: String,
  latestSender: String,
  userIds: [{ type: Schema.Types.ObjectId, ref: 'users' }],
}, {
  timestamps: true,
});
export default mongoose.model('conversations', schema);
