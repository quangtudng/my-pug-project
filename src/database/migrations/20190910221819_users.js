exports.up = function (knex) {
    return knex.schema
    .createTable('users', (table) => {
        table.increments('id').primary();
        table.string('fullName', 255).notNullable();
        table.string('user_avatar', 255).notNullable();
        table.string('email', 255).notNullable().unique();
        table.string('phoneNumber', 255).notNullable();
        table.string('password', 255).notNullable();
        table.string('city', 255).notNullable();
        table.string('description', 255).notNullable();
        table.string('slug', 255).notNullable().unique();
        table.timestamp('createdAt').defaultTo(knex.fn.now());
    });
};

exports.down = function (knex) {
    return knex.schema
    .dropTable('users');
};
