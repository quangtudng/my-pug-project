
exports.up = function(knex) {
  return knex.schema
  .createTable('friendlists', (table) => {
      table.integer('user1_id', 255).notNullable();
      table.foreign('user1_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
      table.integer('user2_id', 255).notNullable();
      table.foreign('user2_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
      table.string('request_id',255).notNullable();
      table.integer('status').notNullable();
      table.timestamp('createdAt').defaultTo(knex.fn.now());
  });
};

exports.down = function(knex) {
  return knex.schema
  .dropTable('friendlists');
};
