$(function () {
    $('#send_message').submit(function (e) {
        e.preventDefault();
        const chat_body = $('.layout .content .chat .chat-body');
        const roomID = $('.open-chat').data('conversation-id');
        let message = $('input[name="message"]').val(); 
        let members = new Array;
        $('input[name="message"]').val('');
        message = $.trim(message);
        chats.forEach(chat => {
          if(chat._id === roomID) {
            chat.userIds.forEach(value => {
              members.push(value.id);
            });
          }
        });
        members = JSON.stringify(members);
        
        if (message) {
          $.ajax({
            type:'POST',
            url:'/send-message',
            data: {
              members,
              roomID,
              message,
            },
            success: function(data) {
              console.log(data);
              $('.layout .content .chat .chat-body .messages').append('<div class="message-item outgoing-message"><div class="message-content">' + message + '</div><div class="message-action">PM 14:25 ' + ('<i class="ti-check"></i>') + '</div></div>');
              $('.open-chat .users-list-body .latest-message').text(`You: ${message}`);
              chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
                cursorcolor: 'rgba(66, 66, 66, 0.20)',
                cursorwidth: "4px",
                cursorborder: '0px'
              }).resize();
              $('.chat-box').first().before($('.open-chat'));
            },
          });
        }
    });
    socket.on('incoming-message', (data) => {
      let incomingMessage = data.message;
      const chat_body = $('.layout .content .chat .chat-body');
      
      if (data.senderInfoFixed.id !== user.id) {
        $('.layout .content .chat .chat-body .messages').append('<div class="message-item ' + '"><p class="message-action mb-0">'+ data.senderInfoFixed.fullName +'</p><div class="message-content">' + incomingMessage + '</div><div class="message-action">PM 14:25 ' + ('<i class="ti-check"></i>') + '</div></div>');
      }
      chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
        cursorcolor: 'rgba(66, 66, 66, 0.20)',
        cursorwidth: "4px",
        cursorborder: '0px'
      }).resize();
    });
    dynamicSocket.on('friend-incoming-message', (data) => {
      $('.chat-box').each(function() {
        if($(this).data('conversation-id') === data.roomID) {
          $('.chat-box').first().before($(this));
          $(this).find('.latest-message').text(`${data.sender.fullName}`+ `: ` +`${data.message}`);
          if(!$(this).hasClass('open-chat')) {
            $(this).find('.latest-message').addClass('font-weight-bold text-dark');
          }
        }
      });
    })
    $(document).on('click', '.layout .content .sidebar-group .sidebar .list-group-item', function () {
        if (jQuery.browser.mobile) {
            $(this).closest('.sidebar-group').removeClass('mobile-open');
        }
    });

});
