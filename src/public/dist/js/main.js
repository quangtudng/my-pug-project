$(document).ready(function() {
  function sendRoomInformation() {
    if(window.location.pathname === '/') {
      if($('.chat-box')[0]) {
          window.location.href = '/t/' + $('.chat-box').first().data('conversation-id');
      }
    }
    else {
      $('.chat-box').each(function() {
        if($(this).data('conversation-id') === window.location.pathname.replace('/t/', '')) {
          $(this).addClass('open-chat');
          socket.emit('joinRoom', $(this).data('conversation-id'));
          dynamicSocket.emit('joinDynamicRoom', user.id);
          check=1;      
        }
      });
    }
  };
  sendRoomInformation();
  // Javascript for Register By Phone
  $('#register-phone-form').submit(function(event) {
      // Recaptcha Process
      window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
      const appVerifier = window.recaptchaVerifier;
      event.preventDefault();

      let phoneNumber = $('input[name="phoneNumber"]').val(); 
      phoneNumber = '+84'+ phoneNumber.substring(1,phoneNumber.length);
      console.log(phoneNumber);
      
      // Create an account on Firebase with phone number
      firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
          .then(function (confirmationResult) {
              window.confirmationResult = confirmationResult;

              const fullName = $('input[name="firstName"]').val() + ' ' + $('input[name="lastName"]').val();

              $('#register-phone-container').remove();
              $('#register-verify-container').css('display', 'block');

              $('#register-verify-form').submit(function(event) {
                  event.preventDefault();
                  const code = $('input[name="verificationCode"]').val();
                  confirmationResult.confirm(code).then(function (result) {
                      userInfo = {
                          fullName,
                          phoneNumber,
                      }
                      $.ajax({
                          type:'POST',
                          url:'/register-phone',
                          data: userInfo,
                          success : function() {
                            alert('Congratulation ! You have created an account');    
                            window.location.replace("/login-phone");  
                          }
                      });
                  }).catch(function (error) {
                      alert('The verification code you entered was incorrect');
                  });
              });
          });
  });
// Javascript for Login By Phone
  $('#login-phone-form').submit(function(event) {
      window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
      const appVerifier = window.recaptchaVerifier;
      event.preventDefault();

      let phoneNumber = $('input[name="phoneNumber"]').val(); 
      phoneNumber = '+84'+ phoneNumber.substring(1,phoneNumber.length);
      
      firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
          .then(function (confirmationResult) {
              window.confirmationResult = confirmationResult;
              $('#login-phone-container').remove();
              $('#login-verify-container').css('display', 'block');
              $('#login-verify-form').submit(function(event) {
                  event.preventDefault();
                  const code = $('input[name="verificationCode"]').val();
                  confirmationResult.confirm(code).then(function (result) {
                      const user=result.user;
                      firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
                          const userInfo = {
                              email: user.email,
                              phoneNumber,
                              idToken,
                          };  
                          $.ajax({
                              type:'POST',
                              url:'/login-phone',
                              data: userInfo,
                              success : function(data) {
                                  if(data == 'good') {              
                                      window.location.href = "/";
                                  }
                                  if(data == 'badToken') {
                                      alert('Error ! User with a bad token');
                                  }
                              }
                          });
                      });
                  });
              });
          });
  });
// Javascript for Register By Email
  $('#register').submit(function(event) {
      event.preventDefault();
      const fullName = $('input[name="firstName"]').val() + ' ' + $('input[name="lastName"]').val();
      const email = $('input[name="email"]').val();
      const password = $('input[name="password"]').val();
      firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
          const user = firebase.auth().currentUser;
          const userInfo = {
              fullName,
              email,
              password,
          };
          if(user) {
              user.sendEmailVerification().then(function() {
                  $.ajax({
                      type:'POST',
                      url:'/register',
                      data: userInfo,
                      success : function() {
                          alert('Congratulation ! You have created an account , please check your email to verify your account');    
                          window.location.replace("/login");  
                      }
                  });
              });
          }
      });
  });
// Javascript for Login By Email
  $('#login').submit(function(event) {
      event.preventDefault();
      const email = $('input[name="email"]').val();
      const password = $('input[name="password"]').val();
      firebase.auth().signInWithEmailAndPassword(email, password).then(function() {
          const user = firebase.auth().currentUser; 
          if(user.emailVerified) {
              firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
                  const userInfo = {
                      email,
                      password,
                      idToken,
                  };  
                  $.ajax({
                      type:'POST',
                      url:'/login',
                      data: userInfo,
                      success : function(data) {
                        if(data == 'good') {
                          window.location.href = "/";
                        }
                        else {
                          alert('An error has occurred, please try again');
                        }
                      }
                  });
              });
          }
          else {
              alert('We are sorry but your account has not been verified yet , please proceed to your email to verify');
          }
      });
  });
// Javascript for Logout
  $('#logout').click(function(event) {
      event.preventDefault();
      firebase.auth().signOut().then(function() {
        window.location.href = "/logout";
      });
  });
// Javascript for getting all current friends with Ajax
  $('.notify_badge').click(function(event) {
      event.preventDefault();
      $.ajax({
        type:'POST',
        url:'/get-current-friend',
        success: function(data) {
          $('.friend-body ul').remove();
          let html = '<ul class="list-group list-group-flush">';
          data.forEach(value => {
            if(value.status == 1) {
              html += "<li class='list-group-item' data-profile=" + value.user2_id+ "><div><figure class='avatar'><img class='rounded-circle' src='/dist/images/mylord.jpg'></figure></div><div class='users-list-body'><h5>" + value.fullName + "</h5><span>"+ value.email + "</span><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='unfriend dropdown-item' href='#' data-profile="+ value.user2_id +">Remove friend</a></div></div></div></div></li>"
            }
          });
          html += "</ul>";
          $('.friend-body').append(html);
        },
      });
  });
// Regex for checking email and number
  function check(info) {
      const regexNumber = /^[0-9]*$/;
      const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(regexNumber.test(String(info))) {
        return "number";
      };
      if(regexEmail.test(String(info))) {
        return "email";
      }
  }
// Javascript for Adding friend
  $('#addFriend').submit(function(event) {
      event.preventDefault();
      const info = $('input[name="info"]').val();
      if(check(info) == "number" || check(info) == "email") {
        $.ajax({
          type:'POST',
          url:'/add-friend',
          data: {
            info,
            type: check(info),
          },
          success: function(data) {
            if(data == 'good') {
              $('#addFriend').modal('hide');
              alert('Request sent!');
            }
            if(data == 'self') {
              alert('You cant be friend with yourself');
            }
            if(data == 'already') {
              alert('You are already friend with this user');
            }
          },
        });
      }
      else {
        console.log('theFuck?');
      }
  });
// Javascript for Accepting friend
  $(document).on('click','.acceptFriend',function(event) {
      event.preventDefault();
      
      const profileID = $(this).attr('data-profile');
      $.ajax({
        type:'POST',
        url:'/accept-friend',
        data: {
          profileID,
        },
        success: function(data) {
          alert('Friend Accepted!');
          $("li[data-profile=" + profileID + "]").remove();
        },
      });
  });
// Javascript for Unfriend
  $(document).on("click",".unfriend",function(event) {
      event.preventDefault();
      const profileID = $(this).attr('data-profile');
      $.ajax({
        type:'POST',
        url:'/unfriend',
        data: {
          profileID,
        },
        success: function(data) {
          alert('Friend Deleted!');
          $("li[data-profile=" + profileID + "]").remove();
        },
      });
  });
// Javascript for getting all current chat sessions with Ajax
  $('.friend_chat').click(function(event) {
      event.preventDefault();
      $.ajax({
        type:'POST',
        url:'/get-current-chat',
        success: function(data) {
          $('.message-body ul').remove();
          let html = '<ul class="list-group list-group-flush">';
          data.chats.forEach(value => { 
              // If the conversation is a private chat
              if(value.userIds.length == 2) {
                // Process through 2 IDs in array to find the person you are chatting to
                for(let i = 0;i < 2; i++) {
                  if(value.userIds[i].id != data.currentUser) {
                    html += "<li class='list-group-item chat-box' data-conversation-id='" + value._id + "'><div><figure class='avatar'><img class='rounded-circle' src='/dist/images/man_avatar3.jpg'></figure></div><div class='users-list-body'><h5>" + value.userIds[i].fullName + "</h5><p class='latest-message'>"+ ((typeof(value.latestMessage) !== 'undefined') ? ((user.fullName == value.latestSender) ? ('You: ' + value.latestMessage) : (value.latestSender + ': ' + value.latestMessage)) : '')+"</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a></div><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item' href='#''data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></li>";
                  }
                }
              }
              // If the conversation is a group chat
              else {
                  html += "<li class='list-group-item chat-box' data-conversation-id='"+ value._id + "'><div><figure class='avatar'><img class='rounded-circle' src='/dist/images/man_avatar3.jpg'></figure></div><div class='users-list-body'><h5>" + value.groupName + "</h5><p class='latest-message'>"+ ((typeof(value.latestMessage) !== 'undefined') ? ((user.fullName == value.latestSender) ? ('You: ' + value.latestMessage) : (value.latestSender + ': ' + value.latestMessage)) : '') +"</p><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item' href='#''data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div></div></li>";
              }
          });
          html += "</ul>";
          $('.message-body').append(html);
          sendRoomInformation();
        },
      });
  });
  $('#editProfileForm').submit(function(event) {
      event.preventDefault();
      let phoneNumber = $('input[name="phoneNumber"]').val();
      phoneNumber = '+84'+ phoneNumber.substring(1,phoneNumber.length);
      const fullName = $('input[name="firstName"]').val() + ' ' + $('input[name="lastName"]').val();
      const city = $('input[name="city"]').val();
      const description = $('textarea[name="description"]').val();
      const user = firebase.auth().currentUser;
      const userInfo = {
        phoneNumber,
        fullName,
        city,
        description,
        uid: user.uid,
        email: user.email,
      }
      $.ajax({
          type:'POST',
          url:'/update-info',
          data: userInfo,
          success: function(data) {
              $('#editProfileModal').modal('hide');
          },
      });
  });
// Javascript for search bar
  $('.searchFullName').keyup(function() {
      let fullName = $(this).html();
      if(fullName.length > 0) {
        $.ajax({
            type:'POST',
            url:'/search-user',
            data: { fullName },
            success: function(data) {
                $('.search_result').remove();
                let html= '<ul class="search_result mt-1 pl-0">';
                let alreadySelected=0;
                for(let i = 0;i < data.searchResults.length;i++) {
                  if(data.searchResults[i].id != data.user) { // Don't show the current user info
                    // Check if the user is already selected!
                    if($('.selected_user').length > 0) {
                      $('.selected_user').each(function() {
                        if($(this).data('selected-id') == data.searchResults[i].id) {
                          alreadySelected = 1;
                        }
                      });
                      // Check for selected!
                      if(alreadySelected == 0) {
                        html+= '<li class="user_tab d-flex" data-user-id='+ data.searchResults[i].id +'><figure class="avatar mr-3"><img class="rounded-circle" src="/dist/images/man_avatar3.jpg"></figure><span class="mt-auto mb-auto user_info">'+ data.searchResults[i].fullName +'</span></li>';
                      }
                      else {
                        html+= '<li class="user_tab d-flex user_tab_active" data-user-id='+ data.searchResults[i].id +'><figure class="avatar mr-3"><img class="rounded-circle" src="/dist/images/man_avatar3.jpg"></figure><span class="mt-auto mb-auto user_info">'+ data.searchResults[i].fullName +'</span></li>';
                        alreadySelected = 0;
                      }
                    }
                    // If there is no selected user then add normally
                    else {
                      html+= '<li class="user_tab d-flex" data-user-id='+ data.searchResults[i].id +'><figure class="avatar mr-3"><img class="rounded-circle" src="/dist/images/man_avatar3.jpg"></figure><span class="mt-auto mb-auto user_info">'+ data.searchResults[i].fullName +'</span></li>';
                    }
                  }
                }
                html+= '</ul>';
                $('.searchFullName').after(html);
            },
        });
      }
      else {
        $('.search_result').remove();
      }
  });
// Javascript for clicking on a member list
  $(document).on('click','.user_tab',function() {
    let count=0;
    $(this).toggleClass('user_tab_active');
    // If the user wants to add a new person
    if($(this).hasClass('user_tab_active')) {
      $('.selected_container').append("<div class='col-3 selected_user' data-selected-id= "+ $(this).data('user-id') +">"+ $(this).text() +"</div>");
    }
    // If the user wants to remove a person
    else {
      const removePerson = $(this).data('user-id');
      $('.selected_user').each(function() {
        if($(this).data('selected-id') == removePerson) {
          $(this).remove();
        }
      });
    }
    // Count the number of active user
    $('.selected_user').each(function() {
        count++;
    });
    if(count >= 2) {
      $('.group-btn').removeAttr('disabled');
    }
    else {
      $('.group-btn').prop('disabled',true);
    }
  });
// Javascript for Creating Group
  $('#createGroup').submit(function(event) {
      event.preventDefault();
      const groupName = $('input[name="groupName"]').val();
      const description = $('textarea[name="description"]').val();
      let users = new Array;
      $('.selected_user').each(function() {
          users.push($(this).data('selected-id'));
      });
      users =  JSON.stringify(users);
      if(users.length >= 2) {
        $.ajax({
            type:'POST',
            url:'/group-add',
            data: {
              users,
              groupName,
              description,
            },
            success: function(data) {
                $('#newGroup').modal('hide');
                window.location.href = "/";
            },
        });
      }
  });
//Javascript for getting all current friend requests
  $('.friend_requests').click(function(event) {
    event.preventDefault();
    $.ajax({
      type:'POST',
      url:'/get-current-request',
      success: function(data) {        
        $('.sidebar-body ul').remove();
        let html = "<ul class='list-group list-group-flush users-list'>";
        data.requests.forEach(request => {
            if(request.status == 0 && request.request_id != data.user.id) {
                  html += "<li class='list-group-item' data-profile=" + request.id +"><div class='users-list-body'><h5>"+ request.email +"</h5><span>Pending</span></div><div class='users-list-action action-toggle'><div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='acceptFriend dropdown-item' href='#' data-profile="+ request.request_id + "> Accept</a></div></div></div></li>";
            }
        });
        html += "</ul>";
        $('.sidebar-body').append(html);
      },
    });
  });
// Javascript for clicking on a chat box
  $(document).on('click','.chat-box',function() {
    window.location.href='/t/' + $(this).data('conversation-id');
  });
  // Retrieve Firebase Messaging object.
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then((currentToken) => {
      if (currentToken) {
        console.log(currentToken);
      } else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
      }
    }).catch((err) => {
      console.log('An error occurred while retrieving token. ', err);
    });
    }
  });
  messaging.onMessage((payload) => {
    console.log('Message received. ', payload);
  });
});
